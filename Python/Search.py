
def BinarySearch(arr, x):
    start, end= 0, len(arr)
    while start <= end:
        mid = (start + end)// 2
        if arr[mid] == x:
            return mid
        if arr[mid] < x: 
            start = mid+1
        elif arr[mid] > x:
            end = mid-1
       
    return -1

print(BinarySearch([1,10, 20, 30, 50, 60, 80, 100, 110, 130, 170], 1))


def LinerSearch(a, x):
    for i in range(len(a)):
        if a[i] == x :
            return i
        
    return -1

print(LinerSearch([10, 20, 30, 50, 60, 80, 100, 110, 130, 170], 100)) 


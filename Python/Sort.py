"""
Bubble Sort
"""
def BubbleSort(arr):
    isSorted = False
    lastUnSorted = len(arr)-1
    while not isSorted:
        isSorted = True
        for i in range(lastUnSorted):
            if arr[i] > arr[i+1]:
                Swap(arr, i, i+1)
                isSorted = False
        lastUnSorted-=1
    return arr

def Swap(arr, IndexA, IndexB):
    arr[IndexA], arr[IndexB] = arr[IndexB], arr[IndexA]

print(BubbleSort([100,20,40,60,10,50,5]))

"""
Insertion Sort
"""

def InsertionSort(arr):
    for i in range(1, len(arr)): 
        key = arr[i] 
        j = i-1
        while j >=0 and key < arr[j] : 
            arr[j+1] = arr[j] 
            j -= 1
        arr[j+1] = key
    return arr

print(InsertionSort([100,20,40,60,10,50,5]))

"""
Merge Sort
"""

def Merge(leftArray, rightArray, array):
    i, j,k = 0, 0, 0
    while i <= len(leftArray)-1 and j<=len(rightArray)-1:
        if leftArray[i] <= rightArray[j]:
            array[k] = leftArray[i]
            i+=1
        else:
            array[k] = rightArray[j]
            j+=1
        k+=1
    while i <=len(leftArray)-1:
        array[k] = leftArray[i]
        k+=1
        i+=1
    while j<=len(rightArray)-1:
        array[k] = rightArray[j]
        k+=1
        j+=1
    
def MergeSort(array):
    arrayLen = len(array)
    if arrayLen == 1:
        return
    mid = arrayLen// 2
    leftArray = array[:mid]
    rightArray = array[mid:]

    MergeSort(leftArray)
    MergeSort(rightArray)

    Merge(leftArray, rightArray, array)
    return array

print(MergeSort([5,6,2,3,8959,323,34,55,5,6]))

"""
QuickSort
"""

def QuickSort(array, start,end):
    if start< end:
        partition = Partition(array, start, end)

        QuickSort(array, start, partition-1)
        QuickSort(array, partition+1, end)

def Partition(array, start, end):
    i = start -1
    pivot = array[end]

    for j in range(start, end):
        if array[j] <= pivot:
            i +=1
            array[i], array[j] = array[j],array[i]
    
    array[i+1], array[end] = array[end], array[i+1]
    return i+1

array = [10, 7, 8, 9, 1, 5] 
arraylen = len(array)-1
QuickSort(array, 0, arraylen)
print(array)
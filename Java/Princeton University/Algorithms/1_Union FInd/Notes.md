# 1. Union Find 

## Quick Find

|Algorithm|Initialize|Union|Find|
|-|-|-|-|
|Quick Find| N|N|1|

Quick find defect : union too expensive


## Quick Union (lazy Approach)
- integer array of size $N$
- Interpretation $id[i]$ is parent of i
- Root of i is $id[id[id[...id[i]...]]]$

|i|0|1|2|3|4|5|6|7|8|9|
|-|-|-|-|-|-|-|-|-|-|-|
|id[]|0|1|9|4|9|6|6|7|8|9|
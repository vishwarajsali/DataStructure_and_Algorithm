/**
 * QuickUnion
 */
public class QuickUnion {

    private int[] id;

    public QuickUnion(int N){
        id = new int[N];
        for(int i = 0; i< N; i++) id[i] = i;
    }

    private int root(int i){
        while(i != id[i]) i = id[i];
        return i;
    }

    public boolean connected(int p , int q){
        return root(p) == root(q);
    }

    public void union(int p, int q){
        int i = root(p);
        int j = root(q);
        id[i] = j;
    }
    public static void main(String[] args) {
        QuickUnion QU = new QuickUnion(10);

        System.out.println(QU.connected(1, 4));
        QU.union(1, 4);
        System.out.println(QU.connected(1, 4));
        System.out.println(QU.connected(4, 8));
        QU.union(4, 8);
        System.out.println(QU.connected(1, 8));

        
        
    }
}
##Algorithms

1. Dynamic Programming - Compute solution for smaller instance of a given problem and use ther solutions to construct a solution to the problem. for example. adding a cache to recursive Fibonacci calculation.
2. Greedy algo. - Compute a solution in stages. making choices that are locally optimum at each step; these choices are never undone.
3. Divide and conquer
   1. break complex into small, often recursive parts.
   2. allow better parallelizarion
   3. must divide the problem into two smaller subprograms, solve each of them recrsively and then meld the two partial solutions into one solution to the full problem. 
   4. Mergsort
4. 
/**
 * Stack
 */
// by  array 
public class Stack {

    private int[] array;
    private int i = -1;

    public Stack(int capacity){
        array = new int[capacity];
    }

    public int size(){
        return i +1;
    }

    public void push(int x){
        if(i == array.length -1) throw new RuntimeException("Stack is full");
        arr[++i] = x;
    }

    public int pop(){
        if(i == -1) throw new RuntimeException("Stack is Empty");
        return array[i--];
    }
}
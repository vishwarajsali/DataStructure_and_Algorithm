import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Graph
 */
public class Edge {

    public int weight;
    public int from;
    public int to;
}

/**
 * Graph
 */
public class Vertex implements Comparable<Vertex> {
    public int id;

    public Set<Edge> edges = new HashSet<>();

    public Vertex parent = null;

    public int distance = Integer.MAX_VALUE; // for dijkstra

    public boolean discovered = false;

    public Vertex(int id){
        this.id = id;
    }
    
    // for dijkstra priority queue

    @Override
    public int compareTo(Vertex o) {
        if(this.distance < o.distance) return -1;
        else if (o.distance < this.distance) return 1;
        else return 0;
    }
}

/**
 * Graph
 */
public class Graph {

    public Map<Integer, Vertex> vertices = new HashMap<>();
}
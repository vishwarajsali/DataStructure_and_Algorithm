/**
 * Queue
 */
public class Queue {

    private int[] arr;
    private int front = -1;
    private int rear = -1;

    public Queue(int capacity){
        arr = new int[capacity];
    }

    public int size(){
        if(rear == -1 && front == -1) return 0;
        else if (rear >= front) return rear - front +1;
        else return arr.length + rear - front +1; 
    }

    public void enqueue(int x){
        if((rear +1) % arr.length == front) throw new RuntimeException("Queue is full");
        
        if(size() == 0) {
            front = 0;
            rear = 0;
            arr[front] = x;

        } else {
            rear = (rear +1) % arr.length;
            arr[rear] = x;
        }
    }

    public int dequeue(){
        if(size() == 0) throw new RuntimeException("Queue is empty");

        int res = arr[front];
        
        if (front == rear) {
            front = -1;
            rear = -1 ;

        }else {
            front = (front +1) % arr.length;
        }

        return res;
    }
}